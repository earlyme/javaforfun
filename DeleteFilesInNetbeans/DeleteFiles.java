import java.io.*;

public class DeleteFiles {
    public static void main(String[] args) {
        try {
            //walk("C://Users//Lenovo//Documents//NetBeansProjects//Chapter2");
            File file = new File("C://Users//Lenovo//Documents//NetBeansProjects//Chapter13");
            dirDelete(file);
        } catch(Exception e) {} 
    }

    public static void dirDelete(File file) {
        File[] files = file.listFiles();
        if(files == null) return;

        for( File f: files ) {
            String s = f.getName();
            s = s.substring(s.length() - 5);
            System.out.println(s);
            //if(!f.getName().equals("src") && !f.getName().equals("test") ) {
            if(!s.equals(".java")) {
                dirDelete(f);
            } else {
              	f.delete();
            }
        }

        file.delete();
    }
}