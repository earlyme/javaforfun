import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Date;
import java.util.ArrayList;

public class SandwichEditor {
    public static void writeToFile( String fileName, int status, ArrayList<Integer> lst ) throws IOException {
        try {
            File file = new File( fileName );
            
            Scanner scan = new Scanner(file);
            
            String text = "";

            String line = scan.nextLine();
            if( status == State. COOK ) {
                int count = Integer. parseInt( line. charAt( line. length( ) - 1 ) + "" );
                count++;
                text += ( line. substring(0, line. length( ) - 1 ) + count + "\n");
            } else {
                text += line + "\n";
            }

            while( scan.hasNextLine()) {
    
                line = scan.nextLine();
                System.out.println( line );
                text += line + "\n";
    
            }
            PrintWriter fileWriter = new PrintWriter( file );
            
            fileWriter. print( text );

            if ( status == State. COOK ) {
                fileWriter. println( "Cooking Date:  " + new Date(). toString() + "\n");
            } else if ( status == State. COMPUTE ) {
                Date date = new Date();
                int sum = 0;

                for( int i: lst ) 
                    sum += i;
                
                fileWriter. println( "Shopping Date: " + date. toString( ));
                fileWriter. println( "\tКолбаса Говяж. По-казахски  " + lst. get(0) + " тг");
                fileWriter. println( "\tСыр		            " + lst. get(1) + " тг");
                fileWriter. println( "\tКетчуп Шашлыч. МАХЕЕВЪ      " + lst. get(2) + " тг");
                fileWriter. println( "\tМайонез \"Три Желанье\"       " + lst. get(3) + " тг");
                fileWriter. println( "\tБатон                       " + lst. get(4) + " тг" + "\n");
                fileWriter. println( "\tTotal:                      " + sum + " тг" + "\n");
            }
            scan.close();
            fileWriter. close( );
        } catch( IOException e ) {
            e. printStackTrace();
        }
        
        
    }
}