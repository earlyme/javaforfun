import java.io.IOException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.geometry.Pos;

public class UserModifier extends Application {
    @Override
    public void start( Stage primaryStage ) {
        BorderPane pane = new BorderPane();
        
        GridPane gridPane = new GridPane( );
        gridPane. setAlignment( Pos.CENTER );
        gridPane.add( new Label("Kolbase (T): "), 0, 0);
        gridPane.add( new TextField( ), 1, 0);

        gridPane.add( new Label("Cheese: "), 0, 1);
        gridPane.add( new TextField( ), 1, 1);

        gridPane.add( new Label("Ketchup: "), 0, 2);
        gridPane.add( new TextField( ), 1, 2);

        gridPane.add( new Label("Mayo: "),0, 3);
        gridPane.add( new TextField( ), 1, 3);

        gridPane.setStyle( "-fx-border-color: black" );
        Button button = new Button( "COMPUTE" );
        gridPane.add(button, 0, 4);
        
        button. setOnAction( e -> {
            try {
                SandwichEditor. writeToFile("Sandwich Review.txt");
            } catch (IOException ex ) {
                ex. printStackTrace();
            }
        });
        pane. setCenter( gridPane );

        Scene scene = new Scene( pane, 400, 300 );
        primaryStage. setScene( scene );
        primaryStage. show( );
    }
}