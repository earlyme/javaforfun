import java.io.*;
import java.util.*;

public class Filewalker {

    public Filewalker() {}

    private ArrayList<String> list = new ArrayList<>();
    
    private double sum = 0.0;
    
    public void walk( String path, boolean byName ) {

        File root = new File( path );
        File[] files = root.listFiles();
        String name;
        double size;
        String gigabytes;
        if (files == null) return;

        for ( File f : files ) {
            if ( f.isDirectory() ) {
                if (!byName) 
                    list.add("(DIR)------------" + f.getName() + "-------------(DIR)\n");
                
                walk( f.getAbsolutePath(), byName );
            } else {
                name = f.getName();
                size = f.length() * 9.31 * Math.pow(10, -10);
                sum += size;

                gigabytes = String.format("%.2f GB", size).replace(',', '.');
                list.add( name.substring(0, name.length() - 4) + " " + gigabytes + "\n");
            }
        }
       
        if(byName) Collections.sort(list);
    }

    public ArrayList<String> getList() {
        return list;
    }

    public double getTotalSize() {
        return sum; 
    }
    public void print() {
        for(String s: list) 
            System.out.println(s);
    }

    public static void main(String[] args) {
        Filewalker fw = new Filewalker();
        try {
            fw.walk("C:\\newMovies", true);
            fw.print();
            System.out.println(fw.getTotalSize() + " GB");
        } catch(Exception e) {}
    }
}