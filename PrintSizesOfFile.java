import java.util.*;
import java.io.*;


public class PrintSizesOfFile {
	public static void main(String[] args) {
		File file = new File("c://Users//Lenovo//Desktop//AllMoviesIHave.txt");

		String line;
		double sum = 0;
		double d;
		try {
			Scanner input = new Scanner(file);

			while(input.hasNext()) {

				line = input.nextLine();
				if(isValid(line)) {
					line = line.substring(0, line.length() - 3);
					d = Double.parseDouble(line.substring(line.length()-4, line.length()));
					sum += d;
					System.out.println(line);
				}
				
			}
		} catch(Exception e) {

		}

		System.out.println(sum);
	}

	public static boolean isValid(String line) {
		if( line.charAt(0) == '/' && line.charAt(1) == '/') return false;
		return true;
	}
}