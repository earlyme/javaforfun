import java.io.File;

public class RenameFilesInDirectory {
	public static void main(String[] args) {
		File file1 = new File("survey.pdf");
		File file2 = new File("new_survey.pdf");
		boolean b = file1.renameTo(file2);

		if(b)
			System.out.println("SUCCESS");
		else
			System.out.println("ERROR!");
  	}
	
}
